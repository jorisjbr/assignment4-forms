/*global require, module */
/**
 * Created by theotheu on 24-12-13.
 */

module.exports = function (app) {
    "use strict";

    /*  user parts
     ---------------
     We create a variable "user" that holds the controller object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/parts.js');

    // RETRIEVE
    app.get('/parts', controller.list);
    app.get('/parts/:_id', controller.detail);

};