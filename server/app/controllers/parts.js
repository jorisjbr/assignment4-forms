/*global require, exports, console, __filename, sort */
/**
 * Created by theotheu on 24-12-13.
 */

var mongoose = require('mongoose'),
    Product = mongoose.model('Product');


// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    "use strict";

    var conditions, fields, sort;

    conditions = {};
    fields = {};

    Product
        .find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

exports.detail = function (req, res) {
    "use strict";

    var conditions, retDoc, i, groupDoc;

    conditions = {
        "product.items": {
            "$elemMatch": {
                "partNumber": req.params._id
            }
        }
    };

    Product
        .find(conditions, {}, {'createdAt': -1})
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
}
;
